<div align="center">
    <h1>Transferencia simultánea de masa & reacción bioquímica en el soporte esférico</h1>
</div>

A continuación se muestra la ecuación diferencial que representa la transferencia simultanea de masa y reacción química dentro del biocatalizador enzimático, donde:

```math
\begin{equation}
    \frac{d^{2} S}{dr^{2}} +\frac{dS}{dr}\frac{2}{r} -\phi ^{2}\frac{\beta *S}{\beta +S} =0
\end{equation}
```

Para el caso del radio inicial $`r=0`$

```math
\begin{equation}\tag{2}
    3\frac{d^{2} S}{dr^{2}} -\phi ^{2}\frac{\beta *S}{\beta +S} =0
\end{equation}
```

Para poder hallar la distribución se utiliza la siguiente distribución con tablas, donde: 

<div align="center"><img style="background: white;" src="Table/Table_1.png"></div>

Se puede ver en la tabla que se supone un valor de $`S_o`$ (naranja)y se modifica hasta que el valor en $`S|_{r=R}`$ sea igual a 1.

Para hacer lo anterior se definen los siguientes invervalos de $`\beta`$ y $`\phi`$ , donde:

```math
\beta =[ 0.01,0.05,0.1,0.5,1,5,10]\tag{3}
```
```math
\phi =[ 1,2,3,5,10,20,25,50,75,100]\tag{4}
```

Para poder realizar el calculo se definió un $`dr=5*10^{-4}`$

Para calcular lo anterior se realizó un algoritmo en *Python*. El código se
encuentra en el siguiente link:


<a href="SvsR.py">
    <p align="center">
        Programa en Python (Concentración Sustrato vs Radio)
    </p>
</a>
<div align="center">
    El programa se llama SvsR.py.py
</div>

Los resultados se muestran a continuación:

<div align="center"><img style="background: white;" src="S-vs-r/Beta-10.0.png"></div>
<div align="center"><img style="background: white;" src="S-vs-r/Beta-5.0.png"></div>
<div align="center"><img style="background: white;" src="S-vs-r/Beta-1.0.png"></div>
<div align="center"><img style="background: white;" src="S-vs-r/Beta-0.5.png"></div>
<div align="center"><img style="background: white;" src="S-vs-r/Beta-0.1.png"></div>
<div align="center"><img style="background: white;" src="S-vs-r/Beta-0.05.png"></div>
<div align="center"><img style="background: white;" src="S-vs-r/Beta-0.01.png"></div>

---

<div align="center">
    <h4>Cálculo del Factor de Efectividad (μ)</h4>
</div>

 Para el calculo del factor de efectividad se tiene la siguiente ecuación:

```math
\eta =\frac{r_{reacción} con Difusividad}{r_{reacción} sin Difusividad} =\frac{r_{obs}}{\frac{\mathbb{r}_{max} \cdotp S_{S}}{K_{M} +S_{S}}} =\frac{3\cdotp \left| \frac{d\overline{S}}{d\overline{r}}\right| _{d\overline{r}}}{\phi ^{2} \cdotp \frac{\beta }{\beta +1}}\tag{5}
```

Donde se tienen los siguientes valores

```math
\phi =\sqrt{\frac{\mathbb{r}_{Max} '}{K_{M} \cdotp \mathcal{D}_{e,S}}} \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \beta =\frac{K_{M}}{S_{S}}\tag{6}
```

Para el cálculo se requiere de la derivada de la concentración en la superficie externa del biocatalizador esférico, donde este valor fue encontrado anteriormente. Para hacer lo anterior se definen los siguientes invervalos de 𝛽 y 𝜙, donde:

```math
\beta =[ 0.01,0.05,0.1,0.5,1,10]\tag{7}
```
```math
\phi=[1,2,4,6,8,10,20,40,60,80,100]\tag{8}
```

Para calcular lo anterior se realizó un algoritmo en Python (El código se encuentra en el siguiente link)

<a href="Ef.py">
    <p align="center">
        Programa en Python (Factor de Efectividad)
    </p>
</a>
<div align="center">
    El programa se llama Ef.py
</div>

El resultado hallado se encuentra a continuación:

<div align="center"><img style="background: white;" src="Ef/Efi.png"></div>