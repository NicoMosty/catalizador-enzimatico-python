# -*- coding: utf-8 -*-
"""
Created on Jan 19 2020
@author: NicoMosty
"""
from os import remove
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
import math

#___CONSTANTES__#
dt=1
Phi=np.array([1,2,4,6,8,10,20,40,60,80,100])

#_____GENERAR VECTOR DE RADIOS____#
Thiele=np.arange(1, 100+1, dt)
Ef=np.zeros(Thiele.size)

def Eficiencia(T):
    return 3/T*((math.cosh(T))/(math.sinh(T))-1/T)

for t in Thiele:
    print(list(Thiele).index(t))
    print(Eficiencia(t))
    Ef[list(Thiele).index(t)]=Eficiencia(t)

# Graficar SvsR 
plt.plot(np.log(Thiele), np.log(Ef))
plt.xlim(0,math.log(100))
plt.ylim(math.log(0.01),math.log(1))
plt.xlabel("Módulo de Thiele")
plt.ylabel("Factor de Efectividad (μ)")

PhiAxis=np.array([1,2,4,6,8,10,20,40,60,80,100])
plt.xticks(np.log(PhiAxis),PhiAxis)
EfAxis=np.array([0.01,0.02,0.04,0.06,0.08,0.1,0.2,0.4,0.6,0.8,1])
plt.yticks(np.log(EfAxis),EfAxis)

plt.legend(loc ="lower left")
plt.savefig("Modelo 1er Grado\Phi^Beta-vs-Ef.png")
plt.show()